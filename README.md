# Highlighter

A study in ReactJS with Redux and Semantic UI. Specifically, react: 16.4.0, redux: 4.0.0, react-redux: 5.0.7, semantic-ui-react: 0.80.2, and semantic-ui-css: 2.3.1

Highlighter is a control subject I use for applied demonstration of differing technologies. Highlights in yellow any text matching the search query. Looking at our sample query [ Et], try using the modifiers to narrow or grow the search. Try searching for words or parts of words. Use the search box or, for desktop users, select or double click words.