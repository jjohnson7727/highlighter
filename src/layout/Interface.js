import React from "react";
import { Grid, Segment } from 'semantic-ui-react'

import SearchBox from "../component/SearchBox";
import ModifierBox from "../component/ModifierBox";
import FontBox from "../component/FontBox";
import '../asset/Interface.css';

const Interface = (props) => {
  return (
    <div className="Interface">
      <Grid>
        <Grid.Row columns={3}>
          <Grid.Column>
            <Segment raised>
              <SearchBox />
            </Segment>
          </Grid.Column>

          <Grid.Column>
            <Segment raised>
              <ModifierBox />
            </Segment>
          </Grid.Column>

          <Grid.Column>
            <Segment raised>
              <FontBox />
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
}

export default Interface;
