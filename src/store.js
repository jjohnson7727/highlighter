import { createStore, combineReducers, applyMiddleware } from 'redux';

import changeActionMiddleware from './kernel/changeActionMiddleware';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import search from './reducer/searchReducer';
import font from './reducer/fontReducer';
import header from './reducer/headerReducer';
import modifier from './reducer/modifierReducer';
import lorem from './reducer/loremReducer';

const store = createStore(
  combineReducers({ search, font, header, modifier, lorem }),
  {/*preloadedState*/},
  applyMiddleware(changeActionMiddleware, logger, thunk)
);

export default store;
