import React from 'react';
import Main from './layout/Main'

/*
 * Maintaining a standard React structure by keeping App.js. But
 * reducing its infulence to that of a passthrough to the main
 * layout component.
 */
const App = (props) => {
  return (
    <Main />
  );
}

export default App;
