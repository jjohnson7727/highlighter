import React from 'react';

import { Divider } from 'semantic-ui-react';
import Header from '../component/Header';
import Intro  from './Intro';
import Lorem  from  '../component/Lorem';

import '../asset/Main.css';

const Main = (props) => {
  return (
    <div className='Main'>
      <Header />
      <br />
      <Intro />
      <Divider />
      <Lorem />
    </div>
  );
}

export default Main;
