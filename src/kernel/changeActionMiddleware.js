import ChangeActionDelegate from './ChangeActionDelegate';
import { searchTally } from '../action/searchAction';
import { loremMarkup } from '../action/loremAction';
import data from '../asset/verbiage.json';

const changeActionMiddleware = (store) => (next) => (action) => {
  interceptChangeTypes(store, action);
  next(action);
}

const interceptChangeTypes = (store, action) => {
  const delegate = getTyped(action.type);

  if(delegate) {
    const payload = delegate(store.getState(), action);
    notify(store, payload);
  }
}

const getTyped = type => {
  let types = {
    'SEARCH_CHANGE': changeDelegate,
    'SEARCH_CLEAR' : clearDelegate,
    'MODIFIER_CASE': caseDelegate,
    'MODIFIER_TRIM': trimDelegate,
    'MODIFIER_WORD': wordDelegate
  }

  return types[type];
}

const changeDelegate = (state, action) => {
  return instanceOfDelegate(action.payload,
                            state.modifier.isCaseOn,
                            state.modifier.isTrimOn,
                            state.modifier.isWordOn);
}

const caseDelegate = (state, action) => {
  return instanceOfDelegate(state.search.value,
                            !state.modifier.isCaseOn,
                            state.modifier.isTrimOn,
                            state.modifier.isWordOn);
}

const trimDelegate = (state, action) => {
  return instanceOfDelegate(state.search.value,
                            state.modifier.isCaseOn,
                            !state.modifier.isTrimOn,
                            state.modifier.isWordOn);
}

const wordDelegate = (state, action) => {
  return instanceOfDelegate(state.search.value,
                            state.modifier.isCaseOn,
                            state.modifier.isTrimOn,
                            !state.modifier.isWordOn);
}

const clearDelegate = (state, action) => {
  return {count: 0, subjectMarkup: data.lorem};
}

const instanceOfDelegate = (v, c, t, w) => {
  const builder = ChangeActionDelegate.instanceWithRawQuery(v)
                                      .withSubject(data.lorem);
  if(c) builder.caseSensitive();
  if(t) builder.trim();
  if(w) builder.word();

  return builder.find();
}

const notify = (store, payload) => {
  store.dispatch(searchTally(payload.count));
  store.dispatch(loremMarkup(payload.subjectMarkup));
}

export default changeActionMiddleware;
