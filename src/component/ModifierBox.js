import React from 'react';
import { connect } from 'react-redux'
import {modifierTrim, modifierWord, modifierCase} from '../action/modifierAction';

import { Button } from 'semantic-ui-react'

import '../asset/ModifierBox.css';
import '../asset/Common.css';

const ModifierBox = (props) => {
  return (
    <div className="ModifierBox">
      <div className="Common-label">Modifiers</div>
      <Button.Group>
        <Button toggle active={props.isTrimOn} onClick={() => props.handleTrim()}>
          Trim
        </Button>
        <Button toggle active={props.isCaseOn} onClick={() => props.handleCase()}>
          Case
        </Button>
        <Button toggle active={props.isWordOn} onClick={() => props.handleWord()}>
          Word
        </Button>
      </Button.Group>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isTrimOn: state.modifier.isTrimOn,
    isCaseOn: state.modifier.isCaseOn,
    isWordOn: state.modifier.isWordOn
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleTrim: () => dispatch(modifierTrim()),
    handleCase: () => dispatch(modifierCase()),
    handleWord: () => dispatch(modifierWord())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModifierBox);
