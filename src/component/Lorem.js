import React from 'react';
import { connect } from 'react-redux';
import { searchChange } from '../action/searchAction';

import '../asset/Lorem.css';

class Lorem extends React.Component  {

  componentDidMount() {
    this.props.handleSelection(' El');
  }

  handleMouseUp(e) {
    const selection = this.getSelectedText();
    if(selection.length > 0) {

      this.props.handleSelection(selection);
    }
    e.stopPropagation();
  }

  getSelectedText() {
      let selObject = null;
      let selectedText = '';

      if(window.getSelection) selObject = window.getSelection();

      if(selObject) {
          selectedText = selObject.toString();
          if(selectedText && '' !== selectedText) selObject.removeAllRanges();
      }

      return selectedText;
  }

  render() {
    return (
      <div className='Lorem'>
        <span style={{fontSize: this.props.fontSize}}
              onMouseUp={(e) => this.handleMouseUp(e)}
              dangerouslySetInnerHTML={{__html: this.props.markup}} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    markup: state.lorem.markup,
    fontSize: state.font.size
   }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSelection: (v) => dispatch(searchChange(v))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Lorem);
