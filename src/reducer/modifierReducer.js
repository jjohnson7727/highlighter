const modifierReducer = (state = {
  isTrimOn: true,
  isCaseOn: false,
  isWordOn: false
}, action) => {
  switch (action.type) {
    case 'MODIFIER_TRIM':
      state = {
        ...state,
        isTrimOn: !state.isTrimOn
      };
      break;
    case 'MODIFIER_CASE':
      state = {
        ...state,
        isCaseOn: !state.isCaseOn
      };
      break;
    case 'MODIFIER_WORD':
      state = {
        ...state,
        isWordOn: !state.isWordOn
      };
      break;
    default:
      break;
  }

  return state;
};

export default modifierReducer;
