export function searchChange(change) {
  return {
    type: 'SEARCH_CHANGE',
    payload: change
  }
}

export function searchTally(tally) {
  return {
    type: 'SEARCH_TALLY',
    payload: tally
  }
}

export function searchClear() {
  return {type: 'SEARCH_CLEAR'};
}
