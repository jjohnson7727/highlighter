import React from 'react';
import PropTypes from 'prop-types';
import '../asset/Common.css';

const Badge = (props) => {
  let badge = props.value === 0 ? null :
    <sup className='Common-badge'>{props.value}</sup>

  return (
    badge
  );
}

Badge.defaultProps = {
  value: 0
};

Badge.propTypes = {
  value: PropTypes.number
}

export default Badge;
