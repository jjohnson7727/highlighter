import React from 'react';
import { connect } from 'react-redux';
import { headerMenu } from '../action/headerAction';
import data from '../asset/verbiage';

import { Button } from 'semantic-ui-react'

import '../asset/Header.css';
import '../asset/Common.css';

const Header = (props) => {
  return (
    <div className='Header Common-background-color'>
      <div className='Header-panel'>
        <h1 className='Header-title'>
          {data.title}
        </h1>
        <Button
          content="Menu"
          icon="content"
          onClick={() => props.handleMenu()} />
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleMenu: () => dispatch(headerMenu())
  }
};

export default connect(null, mapDispatchToProps)(Header);
