const headerReducer = (state = {isMenuOpen: true}, action) => {
  switch (action.type) {
    case "HEADER_MENU":
      state = {
        ...state,
        isMenuOpen: !state.isMenuOpen 
      };
      break;
    default:
      break;
  }

  return state;
};

export default headerReducer;
