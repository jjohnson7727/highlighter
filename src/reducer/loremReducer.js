import verbiage from '../asset/verbiage.json';

const loremReducer = (state = {
    control: verbiage.lorem,
    markup: verbiage.lorem
  },
  action) => {
  switch (action.type) {
    case "LOREM_MARKUP":
      state = {
        ...state,
        markup: action.payload
      };
      break;
    default:
      break;
  }

  return state;
};

export default loremReducer;
