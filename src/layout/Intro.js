import React from 'react';
import { connect } from 'react-redux';

import Interface from './Interface';
import Information from '../component/Information';

import '../asset/Intro.css';

const Intro = (props) => {
  let classes = 'Intro ' + (props.isMenuOpen ? '' : 'Intro-close');

  return (
    <div className={classes}>
      <Information />
      <br />
      <Interface />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isMenuOpen: state.header.isMenuOpen
  }
};

export default connect(mapStateToProps)(Intro);
