import React from 'react';
import { connect } from 'react-redux';
import { searchChange, searchClear } from '../action/searchAction';

import { Icon, Input, Label } from 'semantic-ui-react';
import BoxLabel from './BoxLabel';

import '../asset/SearchBox.css';
import '../asset/Common.css';

const SearchBox = (props) => {
  let close = (
      <Label className='SearchBox-pointer' onClick={() => props.handleClear()}>
        <Icon name='remove' color='red' fitted loading={props.loading}/>
      </Label>
  );

  return (
    <div className='SearchBox'>
      <BoxLabel label='Search' badge={props.tally} />
      <Input placeholder="To highlight..."
             value={props.value}
             label={close}
             labelPosition='right'
             onChange={(e, d) => props.handleChange(d.value)}/>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    value: state.search.value,
    loading: state.search.loading,
    tally: state.search.tally
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleChange: (v) => dispatch(searchChange(v)),
    handleClear: () => dispatch(searchClear())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
