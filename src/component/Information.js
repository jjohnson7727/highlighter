import React from 'react';
import { Image } from 'semantic-ui-react'

import data from '../asset/verbiage';
import linkedin from '../asset/myLinkedinProfile.png';
import '../asset/Information.css';

const Information = (props) => {
  return (
    <div className='Information'>
      <div className='Information-intro'>
        <p>
          <Image className='Information-linkedin'
                 src={linkedin}
                 href='https://www.linkedin.com/in/jesse-alan/' />
          {data.summary}
        </p>
        <p>{data.discription}</p>
      </div>
    </div>
  );
}

export default Information;
