import React from 'react';
import PropTypes from 'prop-types';

import Badge from './Badge'

import '../asset/Common.css';

const BoxLabel = (props) => {
  return (
    <div className='Common-label'>
      {props.label}
      <Badge value={props.badge} additionalClassName="Common-pointer"/>
    </div>
  );
}

BoxLabel.defaultProps = {
  label: '',
  badge: 0
};

BoxLabel.propTypes = {
  label: PropTypes.string,
  badge: PropTypes.number
}

export default BoxLabel;
