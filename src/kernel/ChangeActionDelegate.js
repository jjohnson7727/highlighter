/**
 *  ChangeActionDelegate.js
 *
 *  Author: Jesse A Johnson
 *  Contact: versler.org@gmail.com
 *  Created; June 15, 2014
 *  Revised: March 4, 2018
 *  Version: 2.5
 *  Copyright (C) 2014 2018  Jesse A Johnson of Versler.org
 *
 *  Note: This is ECMAScript 2015 See more on MDN => https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
 *  Usage:
       const text = "The quick brown fox jumps over the lazy dog";
       const indixes = IndexQuery.makeQueryFrom("fox").withSubject(text).find();
 *
 *  License:
       This class file is free software: you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation, either version 3 of the License, or
       (at your option) any later version.

       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.

       The GNU General Public License can be found here: https://www.gnu.org/licenses
 **/
class ChangeActionDelegate {
  constructor() {
    this._rawQuery = null;
    this._pattern = null;
    this._subject = null;
    this._trim = false;
    this._caseSensitive = false;
    this._word = false;
    this._tagClass = 'highlight';
    this._tagName = 'span';
    this._prefix = "<span class='highlight'>";
    this._suffix = "</span>";
    this._mapping = {split : [], match : []};
  }

  /**
    * Creates an instance of HighlightDelegate setting the search query.
    */
  static instanceWithRawQuery(query) {
    const builder = new ChangeActionDelegate();
    builder._rawQuery = query;
    builder._pattern = query;
    return builder;
  }

  /**
    * Over rides the query with new and resets internal state.
    */
  resetWithRawQurey(query) {
    this._rawQuery = query;
    this._pattern = query;
    this._mapping = {split : [], match : []};
    return this;
  }

  /**
    * The data we are searching through.
    */
  withSubject(subject) {
    this._subject = subject;
    return this;
  }

  /**
    * Surrounding tags recieved this class. Defaults to class="highlight"
    */
  withTagClass(tagClass) {
    this.tagClass = tagClass;
    this.updateTag();
    return this;
  }

  /**
    * Data matching the query is surrounded with this tag. Defaults to span.
    */
  withTagName(tagName) {
    this.gatName = tagName;
    this.updateTag();
    return this;
  }

  /**
    * Modifier - Causes query to be trimmed before searching. Defaults to false.
    */
  trim() {
    this._trim = true;
    return this;
  }

  /**
    * Modifier - Causes the search to be case sensitive. Defaults to false.
    */
  caseSensitive() {
    this._scase = true;
    return this;
  }

  /**
    * Modifier - Causes the search to look for words only. Defaults to false.
    */
  word() {
    this._word = true;
    return this;
  }

  /**
    * Modifier - Causes the search to look for words only. Defaults to false.
    */
  find() {
    if(this._isReadyForSplitting()) this._split();
    return this;
  }

  /**
    * Returns the total items found when invoking find().
    */
  get count() {
    return this._mapping.match.length;
  }

  /**
    * Returns a mapping composed of the split and match arrays
    * based on the query, subject, and modifiers
    */
  get mapping() {
    return this._mapping;
  }

  get subjectMarkup() {
    return this._makeDisplayMarkup();
  }

  get query() {
    return this._rawQuery;
  }

  _isReadyForSplitting() {
    return this._subject && '' !== this._subject &&
            this._rawQuery && '' !== this._rawQuery;
  }

  _split() {
    this._removePatternCharacters();
    this._danglingSpaces();
    this._wordBoundry();
    const regex = new RegExp(this._pattern, this._caseSensitivity());
    let matchResult = this._subject.match(regex);
    this._mapping = {
      split : this._subject.split(regex),
      match : null == matchResult ? [] : matchResult
    }
  }

  /**
   *  causes webpackHotDevClient.js warning "Unnecessary..."
   *  but can be safly ignore.
   */
  _removePatternCharacters() {
     // eslint-disable-next-line
     this._pattern = this._pattern.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "");
  }

  _danglingSpaces() {
    if(this._trim) this._pattern = this._pattern.trim();
  }

  _caseSensitivity() {
    return this._scase ? "g" : "gi";
  }

  _wordBoundry() {
    if(this._word) this._pattern = "\\b" + this._pattern + "\\b";
  }

  _makeDisplayMarkup() {
    let dataMarkup = '';
    if(this.count > 0) dataMarkup = this._makeFusedMapping();
    return this._getNonZeroLengthedMarkup(dataMarkup);
  }

  _makeFusedMapping() {
    let dataMarkup = '';
    this._mapping.split.forEach(function(element, index) {
      dataMarkup += element + this._surroundElementWithMarkingTag(this._mapping.match[index]);
    }, this);
    return dataMarkup;
  }

  _surroundElementWithMarkingTag(matchedElement) {
    return matchedElement === undefined ? "" :
      this._prefix + matchedElement + this._suffix;
  }

  _getNonZeroLengthedMarkup(dataMarkup) {
    return dataMarkup.length === 0 ? this._subject : dataMarkup;
  }

  _updateTag() {
    this._prefix = "<" + this._tagName + " class='" + this._tagClass + "'>";
    this._suffix = "</" + this._tagName + ">";
  }
}

export default ChangeActionDelegate;
