export function modifierTrim(value) {
  return {
    type: "MODIFIER_TRIM",
    payload: value
  };
}

export function modifierCase(value) {
  return {
    type: "MODIFIER_CASE",
    payload: value
  };
}

export function modifierWord(value) {
  return {
    type: "MODIFIER_WORD",
    payload: value
  };
}
