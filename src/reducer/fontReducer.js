const fontReducer = (state = {size: 18}, action) => {
  switch (action.type) {
    case "FONT_SIZE":
      state = {
        ...state,
        size: action.payload
      };
      break;
    default:
      break;
  }

  return state;
};

export default fontReducer;
