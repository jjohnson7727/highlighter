export function loremMarkup(v) {
  return {
    type: "LOREM_MARKUP",
    payload: v
  };
}
