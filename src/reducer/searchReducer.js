const searchReducer = (state = {
  value: '',
  loading: false,
  tally: 0
}, action) => {
  switch (action.type) {
    case 'SEARCH_CHANGE':
      state = {
        ...state,
        value: action.payload
      };
      break;
    case 'SEARCH_CLEAR':
      state = {
        ...state,
        value: '',
        loading: false,
        tally: 0
      };
      break;
    case 'SEARCH_TALLY':
      state = {
        ...state,
        tally: action.payload
      }
      break;
    default:
      break;
  }

  return state;
};

export default searchReducer;
