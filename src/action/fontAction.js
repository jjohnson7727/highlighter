export function fontSize(value) {
  return {
    type: "FONT_SIZE",
    payload: value
  };
}
