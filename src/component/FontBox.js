import React, { Component } from 'react';
import { connect } from 'react-redux';
import {fontSize} from '../action/fontAction';

import { Button, Icon } from 'semantic-ui-react'
import BoxLabel from './BoxLabel';

import '../asset/FontBox.css';
import '../asset/Common.css';

class FontBox extends Component {

  handleDecrease() {
    this.update(this.props.size - 1);
  }

  handleIncrease() {
    this.update(this.props.size + 1);
  }

  handleReset() {
    this.update(18);
  }

  update(value) {
    this.props.handleSize(value);
  }

  render() {
    return (
      <div className='FontBox'>
        <BoxLabel label="Font Size" badge={this.props.size} />
        <Button icon onClick={() => this.handleDecrease()}>
          <Icon name='minus' />
        </Button>
        <Button icon onClick={() => this.handleIncrease()}>
          <Icon name='plus' />
        </Button>
        <Button icon onClick={() => this.handleReset()}>
          <Icon name='remove' color='red'/>
        </Button>
      </div>
    )
  };
}

const mapStateToProps = (state) => {
  return { size: state.font.size }
};

const mapDispatchToProps = (dispatch) => {
  return { handleSize: (v) => dispatch(fontSize(v)) }
};

export default connect(mapStateToProps, mapDispatchToProps)(FontBox);
